﻿# BatallaNavalgo

## Build Status
[![Build status](https://ci.appveyor.com/api/projects/status/04fu7cfh8mqo88r6?svg=true)](https://ci.appveyor.com/project/tinchou/algos-iii-batallanavalgo)

## Alcance
Este es el segundo trabajo práctico para la materia Algoritmos y Programación III de la Facultad de Ingeniería de la Universidad de Buenos Aires. Primer cuatrimestre de 2013.

## Integrantes
- De Valais, Ezequiel - <ezequiel.devalais@gmail.com>
- Esteban, Federico - <fede.est@gmail.com>
- González Coll, Martín - <martingonzalezcoll@gmail.com>

## Descripción del trabajo
La Batalla Navalgo es un juego por turnos en el cual juega el jugador contra la computadora.

La computadora coloca sus naves en el tablero, y el jugador debe destruirlas. El jugador no tiene tablero, el juego consiste solamente en destruir las naves de la computadora.