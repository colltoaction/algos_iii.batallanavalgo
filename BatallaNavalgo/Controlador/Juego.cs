using System;
using BatallaNavalgo.Modelo;
using BatallaNavalgo.Modelo.Explosivos;

namespace BatallaNavalgo.Controlador
{
    public class Juego
    {
        Modelo.Juego juego;

        Action actualizarVista;

        Type MinaAPoner;

        public Juego(Modelo.Juego juego, Action actualizarVista)
        {
            this.juego = juego;
            this.actualizarVista = actualizarVista;
        }

        public void SeleccionarMina(Type type)
        {
            MinaAPoner = type;
        }

        public void ManejarClickEnTablero(Posicion posicion)
        {
            if (MinaAPoner == null)
                return;
            if (posicion != null)
            {
                var iExplosivo = (Explosivo)MinaAPoner.GetConstructor(new Type[] { typeof(Posicion) }).Invoke(new object[] { posicion });
                MinaAPoner = null;
                juego.Poner(iExplosivo);
                juego.AvanzarTurno();
                actualizarVista();
            }
        }
    }
}