using System;

namespace BatallaNavalgo.Controlador
{
    public class Boton
    {
        Juego juego;

        Type tipo;

        public Boton(Juego juego, Type tipo)
        {
            this.juego = juego;
            this.tipo = tipo;
        }

        public void Click()
        {
            juego.SeleccionarMina(tipo);
        }
    }
}

