﻿namespace BatallaNavalgo.Vista
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Drawing.Drawing2D;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;
    using System.Windows.Forms;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Controlador;

    public class Juego : Form
    {
        const int cantidadPosicionesPorLado = 10;
        const int ladoPosicion = 45;
        const int separacionPosicion = 3;
        const int separacionTablero = 47;

        Modelo.Juego modelo;

        Controlador.Juego controlador;

        Tablero tablero;

        public Juego(Modelo.Juego modelo)
        {
            InicializarModelo(modelo);
            InicializarControlador();
            InicializarInterfaz();
            Actualizar();
        }

        void Actualizar()
        {
            this.Invalidate();
            tablero.Actualizar();
        }
        
        void InicializarModelo(Modelo.Juego modelo)
        {
            this.modelo = modelo;
            modelo.OnGanado += () => MostrarMensajeYCerrar("Winner.bmp");
            modelo.OnSinPuntosDisponibles += () => MostrarMensajeYCerrar("Loser.bmp");
        }

        void MostrarMensajeYCerrar(string imagePath)
        {
            Actualizar();
            var msj = new Mensaje(imagePath);
            msj.ShowDialog();
            Close();
        }
            
        void InicializarControlador()
        {
            controlador = new Controlador.Juego(modelo, this.Actualizar);
        }

        void DibujarBotonExplosivo(int row, string texto, Type tipo)
        {
            var btnCtr = new Controlador.Boton(controlador, tipo);

            Button btn = new Button();
            btn.Location = new Point(10 * (ladoPosicion + separacionPosicion) + 2 * separacionTablero, row * (ladoPosicion + separacionPosicion));
            btn.Text = texto;
            btn.Click += (s, e) => btnCtr.Click();
            Controls.Add(btn);
        }

        void InicializarInterfaz()
        {
            this.Text = "BatallaNavalgo";
            this.Width = 800;
            this.Height = 600;

            DibujarBotonExplosivo(2, "Disparo", typeof(Disparo));
            
            DibujarBotonExplosivo(3, "Mina simple", typeof(MinaPuntualConRetardo));
            
            DibujarBotonExplosivo(4, "Mina doble", typeof(MinaDobleConRetardo));
            
            DibujarBotonExplosivo(5, "Mina triple", typeof(MinaTripleConRetardo));
            
            DibujarBotonExplosivo(6, "Mina Contacto", typeof(MinaPorContacto));

            Button btnGuardar = new Button();
            btnGuardar.Location = new Point(10 * (ladoPosicion + separacionPosicion) + 2 * separacionTablero, 7 * (ladoPosicion + separacionPosicion));
            btnGuardar.Text = "Guardar";
            btnGuardar.Click += (s, e) =>
            {
                if (Serializador.SerializeJuego(modelo, "juego.xml"))
                {
                    MessageBox.Show("El juego se guardó con éxito!");
                }
                else
                {
                    MessageBox.Show("El juego no se pudo guardar.");
                }
            };
            Controls.Add(btnGuardar);

            tablero = new Tablero(modelo.Tablero, controlador, 45);
            // debo hacer esto desde afuera del tablero para que funcione. Puse números que sé que funcionan.
            tablero.Size = new Size(487, 487);
            tablero.Location = new Point(10, 10);
            Controls.Add(tablero);
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var superficie = e.Graphics;
            using (SolidBrush puntajeBrush = new SolidBrush(Color.Gray))
            using (Font puntajeFont = new Font("Arial", 15f))
            {
                superficie.DrawString("Tenés " + modelo.Puntaje.ToString() + " puntos", puntajeFont, puntajeBrush, new Point(10 * (ladoPosicion + separacionPosicion) + separacionTablero - 8, 1 * (ladoPosicion + separacionPosicion)));
            }
        }
    }
}
        