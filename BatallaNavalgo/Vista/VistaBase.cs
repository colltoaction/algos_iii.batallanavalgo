using System;
using System.Drawing;
using BatallaNavalgo.Modelo;

namespace BatallaNavalgo.Vista
{
    public abstract class VistaBase
    {
        public abstract void Dibujar(Graphics superficie, int ladoPosicion, int separacionPosicion);
    }
}

