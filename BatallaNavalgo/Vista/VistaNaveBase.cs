namespace BatallaNavalgo.Vista
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Naves.Partes;
    using BatallaNavalgo.Modelo;
    
    public class VistaNaveBase : VistaBase
    {
        readonly IEnumerable<VistaParte> partes;

        public VistaNaveBase(NaveBase nave)
        {
            partes = nave.Partes.Select(p => new VistaParte(p)).ToArray();
        }

        public override void Dibujar(Graphics superficie, int ladoPosicion, int separacionPosicion)
        {
            foreach (var parte in partes)
            {
                parte.Dibujar(superficie, ladoPosicion, separacionPosicion);
            }
        }
    }
}

