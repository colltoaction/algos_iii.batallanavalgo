namespace BatallaNavalgo.Vista
{
    using System;
    using System.Drawing;
    using System.Linq;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    
    public class VistaExplosivo : VistaBase
    {
        Explosivo explosivo;

        public VistaExplosivo(Explosivo explosivo)
        {
            this.explosivo = explosivo;
        }

        public override void Dibujar(Graphics superficie, int ladoPosicion, int separacionPosicion)
        {
            var brush = explosivo.Estado.Explotable ? new SolidBrush(Color.FromArgb(102, Color.DarkRed)) : new SolidBrush(Color.FromArgb(102, Color.Red));
            
            var minX = explosivo.Area.Posiciones.Min(p => p.X);
            var minY = explosivo.Area.Posiciones.Min(p => p.Y);
            var maxX = explosivo.Area.Posiciones.Max(p => p.X);
            var maxY = explosivo.Area.Posiciones.Max(p => p.Y);
            var rect = new RectangleF(minX * (ladoPosicion + separacionPosicion) + separacionPosicion + 2,
                                      minY * (ladoPosicion + separacionPosicion) + separacionPosicion + 2,
                                      (maxX - minX + 1) * ladoPosicion + (maxX - minX) * separacionPosicion,
                                      (maxY - minY + 1) * ladoPosicion + (maxY - minY) * separacionPosicion);
            superficie.FillEllipse(brush, rect);
            superficie.DrawEllipse(Pens.DarkRed, rect);

            brush.Dispose();
        }
    }
}

