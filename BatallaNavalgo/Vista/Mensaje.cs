namespace BatallaNavalgo.Vista
{
    using System.Windows.Forms;
    using System.Drawing;

    public class Mensaje : Form
    {
        string imagePath;

        public Mensaje(string imagePath)
        {
            this.imagePath = imagePath;
            this.Load += (s, e) => Inicializar();
        }

        void Inicializar()
        {
            ClientSize = new System.Drawing.Size(400, 300);

            var thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            var file = thisExe.GetManifestResourceStream("BatallaNavalgo.Vista." + this.imagePath);
            Image img = Image.FromStream(file);

            PictureBox pictureBox = new PictureBox();
            pictureBox.Size = new Size(400, 300);
            pictureBox.Location = new Point(0, 0);
            pictureBox.BorderStyle = BorderStyle.FixedSingle;
            pictureBox.SizeMode = PictureBoxSizeMode.Normal;
            pictureBox.Image = img;

            Button OK = new Button();
            OK.Location = new Point(172, 270);
            OK.Text = "Okay";
            OK.DialogResult = DialogResult.OK;

            Controls.Add(OK);
            Controls.Add(pictureBox);
            this.Name = "Se acabó el juego";
            this.Text = "Se acabó el juego";
            AcceptButton = OK;
        }
    }
}

