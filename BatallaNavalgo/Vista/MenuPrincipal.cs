﻿namespace BatallaNavalgo.Vista
{
    using System.Windows.Forms;
    using System.Drawing;
    using BatallaNavalgo.Modelo;

    public partial class MenuPrincipal : Form
    {
        public MenuPrincipal()
        {
            this.Load += (s, e) => Inicializar();
        }

        void Inicializar()
        {
            this.Text = "Menú principal";

            Button btnNueva = new Button();
            btnNueva.Location = new Point(207, 70);
            btnNueva.Text = "Nueva partida";
            btnNueva.Click += (s, e) => 
            {
                var modelo = new Modelo.Juego();
                modelo.ColocarNaves();
                CrearVista(modelo);
            };
            Controls.Add(btnNueva);

            Button btnCargar = new Button();
            btnCargar.Location = new Point(207, 148);
            btnCargar.Text = "Cargar";
            btnCargar.Click += (s, e) => CrearVista(Serializador.DeserializarDeXml("juego.xml"));
            Controls.Add(btnCargar);

            var thisExe = System.Reflection.Assembly.GetExecutingAssembly();
            var file = thisExe.GetManifestResourceStream("BatallaNavalgo.Vista.Logo.png");
            Image img = Image.FromStream(file);

            PictureBox pictureBox = new PictureBox();
            pictureBox.Size = this.Size;
            pictureBox.Location = new Point(10, 10);
            pictureBox.SizeMode = PictureBoxSizeMode.Normal;
            pictureBox.Image = img;
            
            Controls.Add(pictureBox);
        }

        void CrearVista(Modelo.Juego modelo)
        {
            Hide();
            var vistaJuego = (new Juego(modelo));
            vistaJuego.FormClosed += (s, e) => Show();
            vistaJuego.Show();
            
        }
    }
}