namespace BatallaNavalgo.Vista
{
    using System;
    using System.Drawing;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves.Partes;
    
	public class VistaParte : VistaBase
	{
        ParteBase parte;

        Func<Color> CalcularColor;

        Posicion posAnterior = Posicion.Cero;
        Posicion aSumar = Posicion.Cero;

        public VistaParte(ParteBase parte)
        {
            this.parte = parte;
            posAnterior = parte.Posicion;

            if (parte is ParteDeRompehielos)
            {
                var p = (ParteDeRompehielos)parte;
                CalcularColor = () =>
                {
                    return p.RecibioPrimerDisparo ? Color.FromArgb(102, Color.Green) : Color.FromArgb(102, Color.LimeGreen);
                };
            }
            else if (parte is ParteDeDestructor)
            {
                CalcularColor = () =>
                {
                    return Color.FromArgb(102, Color.PaleGreen);
                };
            }
            else
            {
                CalcularColor = () =>
                {
                    return Color.FromArgb(102, Color.Green);
                };
            }
        }

        public override void Dibujar(Graphics superficie, int ladoPosicion, int separacionPosicion)
        {
            if (RendererFluido.PorcentajeDeMovimiento == 0)
            {
                if (parte.Posicion != posAnterior)
                {
                    aSumar = parte.Posicion + (posAnterior * -1);
                }
            }
            else if (RendererFluido.PorcentajeDeMovimiento == 100)
            {
                posAnterior = parte.Posicion;
                aSumar = Posicion.Cero;
            }

            var brush = parte.Destruida ? new SolidBrush(Color.FromArgb(102, Color.Gray)) : new SolidBrush(CalcularColor());

            var posicion = parte.Posicion;
            //* RendererFluido.PorcentajeDeMovimiento
            superficie.FillRectangle(brush, new RectangleF((posAnterior.X + aSumar.X * RendererFluido.PorcentajeDeMovimiento * 0.01f) * (ladoPosicion + separacionPosicion) + separacionPosicion + 2,
                                                           (posAnterior.Y + aSumar.Y * RendererFluido.PorcentajeDeMovimiento * 0.01f) * (ladoPosicion + separacionPosicion) + separacionPosicion + 2,
                                                           ladoPosicion,
                                                           ladoPosicion));

            brush.Dispose();
        }
	}
}

