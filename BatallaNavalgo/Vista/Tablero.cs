namespace BatallaNavalgo.Vista
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using BatallaNavalgo.Modelo;
    
    public class Tablero : Panel
    {
        const int cantidadPosicionesPorLado = 10;
        readonly int ladoCasillero;
        readonly int separacionEntreCasilleros;

        Modelo.Tablero tablero;

        Controlador.Juego controlador;

        IEnumerable<VistaExplosivo> explosivos = new VistaExplosivo[]{};

        IEnumerable<VistaNaveBase> naves = new VistaNaveBase[]{};

        public Tablero(Modelo.Tablero tablero, Controlador.Juego controlador, int ladoCasillero)
        {
            this.DoubleBuffered = true;

            this.tablero = tablero;
            this.controlador = controlador;
            this.ladoCasillero = ladoCasillero;
            this.separacionEntreCasilleros = ladoCasillero / 15;

            // No encontré un evento Load como tiene Form para disparar esto.
            InicializarInterfaz();
        }

        void InicializarInterfaz()
        {
            this.Width = 500;
            this.Height = 400;

            MouseClick += (s, e) => controlador.ManejarClickEnTablero(EnTablero(e.Location));

            naves = tablero.Naves.Select(n => new VistaNaveBase(n)).ToArray();
            explosivos = tablero.Explosivos.Select(e => new VistaExplosivo(e));
        }

        public void Actualizar()
        {
            System.Threading.Tasks.Task.Factory.StartNew(ActualizarAsync);
        }

        void ActualizarAsync()
        {
            RendererFluido.PorcentajeDeMovimiento = 0;
            this.Invalidate();
            while (RendererFluido.PorcentajeDeMovimiento < 100)
            {
                System.Threading.Thread.Sleep(20);
                // Esta ecuación se llama Linear Interpolation
                RendererFluido.PorcentajeDeMovimiento = (int)Math.Ceiling(RendererFluido.PorcentajeDeMovimiento + (100 - RendererFluido.PorcentajeDeMovimiento) * 0.3);
                this.Invalidate();
                System.Threading.Thread.Sleep(20);
            }
            RendererFluido.PorcentajeDeMovimiento = 0;
        }

        protected override void OnPaint(PaintEventArgs e)
        {
            base.OnPaint(e);

            var superficie = e.Graphics;
            using (Pen blackPen = new Pen(Color.Black, 2))
            {
                superficie.Clear(Color.White);

                Rectangle rec1 = new Rectangle(new Point(2, 2), this.Size - new Size(2, 2));
                superficie.DrawRectangle(blackPen, rec1);

                DibujarExplosivos(superficie);
                DibujarNaves(superficie);
            }
        }

        void DibujarExplosivos(Graphics superficie)
        {
            foreach (var explosivo in explosivos)
            {
                explosivo.Dibujar(superficie, ladoCasillero, separacionEntreCasilleros);
            }
        }

        void DibujarNaves(Graphics superficie)
        {
            foreach (var nave in naves)
            {
                nave.Dibujar(superficie, ladoCasillero, separacionEntreCasilleros);
            }
        }

        Posicion EnTablero(Point location)
        {
            // rectifico por offset del marco del tablero
            location = new Point(location.X - separacionEntreCasilleros - 2, location.Y - separacionEntreCasilleros - 2);//

            return (location.X % (ladoCasillero + separacionEntreCasilleros) < ladoCasillero &&
                    location.Y % (ladoCasillero + separacionEntreCasilleros) < ladoCasillero &&
                    location.X / (ladoCasillero + separacionEntreCasilleros) < cantidadPosicionesPorLado &&
                    location.Y / (ladoCasillero + separacionEntreCasilleros) < cantidadPosicionesPorLado) ?
                new Posicion(location.X / (ladoCasillero + separacionEntreCasilleros), location.Y / (ladoCasillero + separacionEntreCasilleros)) :
                    null;
        }
    }
}

