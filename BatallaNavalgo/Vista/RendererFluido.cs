using System;

namespace BatallaNavalgo.Vista
{
    public static class RendererFluido
    {
        public static int PorcentajeDeMovimiento // 0 a 100
        {
            get;
            set;
        }

        static RendererFluido()
        {
            PorcentajeDeMovimiento = 0;
        }
    }
}

