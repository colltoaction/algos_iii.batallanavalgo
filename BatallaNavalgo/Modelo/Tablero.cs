namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using Explosivos;
    using Naves;

    [Serializable]
    [XmlRoot("Tablero")]
    public class Tablero
    {
        [XmlArray("explosivos")]
        [XmlArrayItem("explosivo")]
        List<Explosivo> explosivos = new List<Explosivo>();

        [XmlArray("naves")]
        [XmlArrayItem("nave")]
        List<NaveBase> naves = new List<NaveBase>();

        public event Action OnTodasLasNavesDestruidas;
        
        public Area Limites
        {
            get
            {
                return Area.FromPosiciones(Posicion.Cero, Posicion.NorEste * 9);
            }
        }

        public List<NaveBase> Naves
        {
            get
            {
                return naves;
            }

            set
            {
                this.naves = value;
            }
        }

        public List<Explosivo> Explosivos
        {
            get
            {
                return explosivos;
            }

            set
            {
                this.explosivos = value;
            }
        }

        // TODO restringir este método a la etapa previa al inicio del juego
        public void Poner(NaveBase nave)
        {
            this.naves.Add(nave);
        }

        public void Poner(Explosivo explosivo)
        {
            this.explosivos.Add(explosivo);
        }

        public void EvaluarColisiones()
        {
            foreach (var explosivo in explosivos)
            {
                if (!explosivo.Estado.Explotable)
                {
                    continue;
                }

                foreach (var nave in naves)
                {
                    if (nave.Area.Interseca(explosivo.Area))
                    {
                        explosivo.ExplotarEn(nave);
                    }
                }
            }

            if (naves.All(n => n.Destruida))
            {
                if (OnTodasLasNavesDestruidas != null)
                {
                    OnTodasLasNavesDestruidas.Invoke();
                }
            }
        }

        public void ActualizarEstadoExplosivos()
        {
            foreach (var explosivo in explosivos)
            {
                explosivo.AvanzarEstado();
            }

            explosivos.RemoveAll(explosivo => explosivo.Explotado);
        }

        public void MoverNaves()
        {
            foreach (var nave in naves.Where(n => !n.Destruida))
            {
                nave.Mover();
            }
        }
    }
}