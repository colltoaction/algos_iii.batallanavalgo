﻿namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    public static class Serializador
    {
        public static Juego DeserializarDeXml(String filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                return default(Juego);
            }

            try
            {
                var serializer = new XmlSerializer(typeof(Juego));
                using (var fs = new FileStream(filename, FileMode.Open))
                {
                    return (Juego)serializer.Deserialize(fs);
                }
            }
            catch (Exception)
            {
                return default(Juego);
            }
        }

        public static bool SerializeJuego(Juego juego, String filename)
        {
            if (juego == null)
            {
                return false;
            }

            try
            {
                XmlSerializer xmlserializer = new XmlSerializer(typeof(Juego));
                Stream stream = new FileStream(filename, FileMode.Create);
                xmlserializer.Serialize(stream, juego);
                stream.Close();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
