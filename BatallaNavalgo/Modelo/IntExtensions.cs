namespace BatallaNavalgo.Modelo
{
    public static class IntExtensions
    {
        public static bool Entre(this int n, int min, int max)
        {
            return n >= min && n <= max;
        }
    }
}