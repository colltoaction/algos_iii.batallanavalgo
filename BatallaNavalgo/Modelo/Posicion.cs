namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Posicion : IEquatable<Posicion>
    {
        public Posicion()
        {
        }

        public Posicion(int x, int y)
        {
            this.X = x;
            this.Y = y;
        }

        public static Posicion Cero
        {
            get
            {
                return new Posicion(0, 0);
            }
        }

        public static Posicion Este
        {
            get
            {
                return new Posicion(1, 0);
            }
        }

        public static Posicion Oeste
        {
            get
            {
                return Posicion.Este * -1;
            }
        }

        public static Posicion Norte
        {
            get
            {
                return new Posicion(0, 1);
            }
        }

        public static Posicion Sur
        {
            get
            {
                return Posicion.Norte * -1;
            }
        }

        public static Posicion NorOeste
        {
            get
            {
                return Posicion.Norte + Posicion.Oeste;
            }
        }

        public static Posicion NorEste
        {
            get
            {
                return Posicion.Norte + Posicion.Este;
            }
        }

        public static Posicion SurEste
        {
            get
            {
                return Posicion.Sur + Posicion.Este;
            }
        }

        public static Posicion SurOeste
        {
            get
            {
                return Posicion.Sur + Posicion.Oeste;
            }
        }

        [XmlElement("X")]
        public int X { get; set; }

        [XmlElement("Y")]
        public int Y { get; set; }

        public static Posicion operator +(Posicion este, Posicion otro)
        {
            return new Posicion(este.X + otro.X, este.Y + otro.Y);
        }

        public static Posicion operator *(Posicion este, Int32 entero)
        {
            return new Posicion(este.X * entero, este.Y * entero);
        }

        public static bool operator ==(Posicion este, Posicion otro)
        {
            if (ReferenceEquals(este, otro))
            {
                return true;
            }

            if (ReferenceEquals(otro, null))
            {
                return false;
            }

            return este.X == otro.X && este.Y == otro.Y;
        }

        public static bool operator !=(Posicion este, Posicion otro)
        {
            return !(este == otro);
        }

        public bool A45Grados(Posicion otra)
        {
            return Math.Abs(this.X - otra.X) == Math.Abs(this.Y - otra.Y);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Posicion);
        }

        public bool Equals(Posicion otro)
        {
            return this == otro;
        }

        // ver http://msdn.microsoft.com/en-us/library/dd183755.aspx
        public override int GetHashCode()
        {
            return this.X ^ this.Y;
        }
    }
}