namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    [Serializable]
    public class Area : IEquatable<Area>
    {
        public Area()
        {
        }

        public Area(List<Posicion> posiciones)
        {
            // lo convierto para evitar que se modifique el área cuando muevo una nave
            // cuando una nave me pasa un enumerable de sus posiciones, por ejemplo.
            this.Posiciones = posiciones;
        }

        public List<Posicion> Posiciones { get; set; }

        public static Area FromPosiciones(Posicion pos1, Posicion pos2)
        {
            var surOeste = new Posicion(Math.Min(pos1.X, pos2.X), Math.Min(pos1.Y, pos2.Y));
            var norEste = new Posicion(Math.Max(pos1.X, pos2.X), Math.Max(pos1.Y, pos2.Y));

            Func<List<Posicion>> posiciones = () => 
            {
                var p = new List<Posicion>();
                for (var i = surOeste.X; i <= norEste.X; i++)
                {
                    for (var j = surOeste.Y; j <= norEste.Y; j++)
                    {
                        p.Add(new Posicion(i, j));
                    }
                }

                return p;
            };
            return new Area(posiciones());
        }

        public static Area FromPosicionesDiagonales(Posicion pos1, Posicion pos2)
        {
            Func<List<Posicion>> posiciones = () => 
            {
                var p = new List<Posicion>();
                var resta = pos2 + (pos1 * -1);
                var dirX = Math.Sign(resta.X);
                var dirY = Math.Sign(resta.Y);
                var j = 0;
                for (var i = 0; Math.Abs(i) <= Math.Abs(resta.X);)
                {
                    p.Add(pos1 + new Posicion(i, j));
                    i += dirX;
                    j += dirY;
                }

                return p;
            };
            return new Area(posiciones());
        }

        public static Area operator +(Area esta, Posicion pos)
        {
            return esta.Desplazada(pos);
        }

        public static bool operator ==(Area esta, Area otra)
        {
            if (ReferenceEquals(otra, null))
            {
                return false;
            }
            
            return !esta.Posiciones.Union(otra.Posiciones).Except(esta.Posiciones.Intersect(otra.Posiciones)).Any();
        }

        public static bool operator !=(Area esta, Area otra)
        {
            return !(esta == otra);
        }

        public bool Interseca(Area otra)
        {
            return Posiciones.Any(p => otra.Contiene(p));
        }

        public bool Contiene(Area otra)
        {
            return otra.Posiciones.All(Contiene);
        }

        public bool Contiene(Posicion pos)
        {
            return Posiciones.Any(p => p == pos);
        }

        public override bool Equals(object obj)
        {
            return this.Equals(obj as Area);
        }

        public bool Equals(Area otra)
        {
            return this == otra;
        }

        // http://www.brpreiss.com/books/opus6/html/page224.html
        public override int GetHashCode()
        {
            return base.GetHashCode() + Posiciones.Sum(pos => pos.GetHashCode());
        }

        Area Desplazada(Posicion pos)
        {
            return new Area(Posiciones.Select(p => p + pos).ToList<Posicion>());
        }
    }
}
