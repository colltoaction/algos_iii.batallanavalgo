namespace BatallaNavalgo.Modelo.Explosivos
{
    using BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos;
    using BatallaNavalgo.Modelo.Naves;

    public class Disparo : Explosivo
    {
        public Disparo()
        {
        }

        public Disparo(Posicion posicion)
        {
            this.Area = Area.FromPosiciones(posicion, posicion);
            this.Estado = new Activo();
        }

        public override int Costo
        {
            get
            {
                return 200;
            }
        }
    }
}