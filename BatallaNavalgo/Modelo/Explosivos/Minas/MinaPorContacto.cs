﻿namespace BatallaNavalgo.Modelo.Explosivos.Minas
{
    using EstadosExplosivos;
    using Naves;

    public class MinaPorContacto : MinaSubmarinaBase
    {
        public MinaPorContacto()
        {
        }

        public MinaPorContacto(Posicion posicion)
        {
            Area = Area.FromPosiciones(posicion, posicion);
            this.Estado = new NoExplotado();
        }

        public override int Costo
        {
            get
            {
                return 150;
            }
        }

        public override void ExplotarEn(NaveBase nave)
        {
            nave.Recibir(this, this.Area);

            // cuando avance el turno se va a poner en Explotado
            this.Estado = new Activo();
        }
    }
}
