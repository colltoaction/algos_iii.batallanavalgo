﻿namespace BatallaNavalgo.Modelo.Explosivos.Minas
{
    using EstadosExplosivos;

    public class MinaPuntualConRetardo : MinaSubmarinaBase
    {
        public MinaPuntualConRetardo()
        {
        }

        public MinaPuntualConRetardo(Posicion posicion)
        {
            this.Area = Area.FromPosiciones(posicion, posicion);
            this.Estado = new InactivoPorDosTurnos();
        }

        public override int Costo
        {
            get
            {
                return 50;
            }
        }
    }
}
