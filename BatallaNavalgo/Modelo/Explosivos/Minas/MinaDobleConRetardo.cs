﻿namespace BatallaNavalgo.Modelo.Explosivos.Minas
{
    using EstadosExplosivos;

    public class MinaDobleConRetardo : MinaSubmarinaBase
    {
        public MinaDobleConRetardo()
        {
        }

        public MinaDobleConRetardo(Posicion posicion)
        {
            this.Area = Area.FromPosiciones(posicion + Posicion.NorEste, posicion + Posicion.SurOeste);
            this.Estado = new InactivoPorDosTurnos();
        }

        public override int Costo
        {
            get
            {
                return 100;
            }
        }
    }
}