﻿namespace BatallaNavalgo.Modelo.Explosivos.Minas
{
    using EstadosExplosivos;

    public class MinaTripleConRetardo : MinaSubmarinaBase
    {
        public MinaTripleConRetardo()
        {
        }

        public MinaTripleConRetardo(Posicion posicion)
        {
            this.Area = Area.FromPosiciones(posicion + Posicion.NorEste * 2, posicion + Posicion.SurOeste * 2);
            this.Estado = new InactivoPorDosTurnos();
        }

        public override int Costo
        {
            get
            {
                return 125;
            }
        }
    }
}
