﻿namespace BatallaNavalgo.Modelo.Explosivos
{
    using System;
    using System.Xml.Serialization;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos;
    using BatallaNavalgo.Modelo.Naves;

    [Serializable]
    [XmlRootAttribute("Explosivo")]
    [XmlInclude(typeof(Disparo))]
    [XmlInclude(typeof(MinaSubmarinaBase))]
    [XmlInclude(typeof(MinaPorContacto))]
    [XmlInclude(typeof(MinaDobleConRetardo))]
    [XmlInclude(typeof(MinaTripleConRetardo))]
    [XmlInclude(typeof(MinaPuntualConRetardo))]
    public abstract class Explosivo
    {
        public Explosivo()
        {
        }

        [XmlElement("Estado")]
        public EstadoExplosivo Estado
        {
            get;
            set;
        }

        [XmlElement("Area")]
        public Area Area
        {
            get;
            set;
        }

        [XmlElement("Costo")]
        public abstract int Costo { get; }

        [XmlElement("Explotado")]
        public virtual bool Explotado
        {
            get
            {
                return this.Estado.Explotado;
            }

            set
            {
            }
        }

        public virtual void ExplotarEn(NaveBase nave)
        {
            if (Estado.Explotable)
            {
                nave.Recibir(this, this.Area);
            }
        }

        public void AvanzarEstado()
        {
            this.Estado = this.Estado.Siguiente();
        }
    }
}
