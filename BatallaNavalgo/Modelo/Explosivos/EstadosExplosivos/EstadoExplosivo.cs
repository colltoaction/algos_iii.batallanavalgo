﻿namespace BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Xml.Serialization;

    [Serializable]
    [XmlRootAttribute("EstadoExplosivo")]
    [XmlInclude(typeof(Activo))]
    [XmlInclude(typeof(Exploted))]
    [XmlInclude(typeof(InactivoPorDosTurnos))]
    [XmlInclude(typeof(InactivoPorUnTurno))]
    [XmlInclude(typeof(NoExplotado))]
    public abstract class EstadoExplosivo
    {
        [XmlElement("Explotado")]
        public abstract bool Explotado { get; set; }

        [XmlElement("Explotable")]
        public abstract bool Explotable { get; set; }

        public abstract EstadoExplosivo Siguiente();
    }
}
