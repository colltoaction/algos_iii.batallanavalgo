﻿namespace BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Exploted : EstadoExplosivo
    {
        public override bool Explotado
        {
            get
            {
                return true;
            }

            set 
            {
            }
        }

        public override bool Explotable
        {
            get
            {
                return false;
            }

            set
            {
            }
        }

        public override EstadoExplosivo Siguiente()
        {
            return this;
        }
    }
}
