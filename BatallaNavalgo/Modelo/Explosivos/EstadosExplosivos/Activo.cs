﻿namespace BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class Activo : EstadoExplosivo
    {
        public override bool Explotado
        {
            get
            {
                return false;
            }

            set
            {
            }
        }

        public override bool Explotable
        {
            get
            {
                return true;
            }

            set 
            {
            }
        }

        public override EstadoExplosivo Siguiente()
        {
            return new Exploted();
        }
    }
}
