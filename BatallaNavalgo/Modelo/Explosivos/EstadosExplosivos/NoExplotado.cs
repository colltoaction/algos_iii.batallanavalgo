﻿namespace BatallaNavalgo.Modelo.Explosivos.EstadosExplosivos
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class NoExplotado : EstadoExplosivo
    {
        public override bool Explotado
        {
            get
            {
                return false;
            }

            set
            {
            }
        }

        public override bool Explotable
        {
            get
            {
                return true;
            }

            set
            {
            }
        }

        // para pasar a explotado se maneja desde la mina que lo contiene
        public override EstadoExplosivo Siguiente()
        {
            return this;
        }
    }
}