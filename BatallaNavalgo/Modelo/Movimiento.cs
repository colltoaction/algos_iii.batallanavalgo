namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Xml.Serialization;

    [Serializable]
    public class Movimiento
    {
        Area limites;
        
        Posicion dir;

        // para serialización
        public Movimiento()
        {
        }

        public Movimiento(Area limites, Posicion dir)
        {
            this.limites = limites;
            this.dir = dir;
        }
        
        // para serialización
        public Area Limites
        {
            get { return limites; }
            set { limites = value; }
        }
        
        // para serialización
        public Posicion Direccion
        {
            get { return dir; }
            set { dir = value; }
        }

        public Posicion Siguiente(Area ocupada, Posicion pos)
        {
            if (!limites.Contiene(ocupada))
            {
                throw new FueraDeLosLimitesException();
            }

            if (!limites.Contiene(ocupada + new Posicion(dir.X, 0)))
            {
                dir = new Posicion(-dir.X, dir.Y);
            }

            if (!limites.Contiene(ocupada + new Posicion(0, dir.Y)))
            {
                dir = new Posicion(dir.X, -dir.Y);
            }

            var newPos = pos + dir;
            return newPos;
        }
    }
}