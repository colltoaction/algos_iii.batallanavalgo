namespace BatallaNavalgo.Modelo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using System.Xml.Serialization;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Naves;

    public class Juego
    {
        const int CostoTurno = 10;
      
        readonly Posicion[] direccionesPosibles = new Posicion[]
        {
            Posicion.Norte,
            Posicion.Oeste,
            Posicion.Sur,
            Posicion.Este
        };

        readonly Posicion[] movimientosPosibles = new Posicion[]
        {
            Posicion.Norte,
            Posicion.NorOeste,
            Posicion.Oeste,
            Posicion.SurOeste,
            Posicion.Sur,
            Posicion.SurEste,
            Posicion.Este,
            Posicion.NorEste
        };

        Tablero tablero;

        public Juego()
            : this(10000)
        {
        }

        public Juego(int puntos)
        {
            this.Puntaje = puntos;
            this.Tablero = new Tablero();
            this.OnSinPuntosDisponibles += () => this.Terminado = true;
        }
        
        public event Action OnGanado;

        public event Action OnSinPuntosDisponibles;

        public int Puntaje { get; set; }

        public Tablero Tablero
        {
            get
            {
                return tablero;
            }

            set
            {
                tablero = value;
                tablero.OnTodasLasNavesDestruidas += () =>
                {
                    this.Terminado = true;
                    if (OnGanado != null)
                        OnGanado.Invoke();
                };
            }
        }
        
        public bool Terminado { get; set; }
        
        public void ColocarNaves()
        {
            var r = new Random();
            Tablero.Poner(NuevaLanchaEnLimites(r));
            Tablero.Poner(NuevaLanchaEnLimites(r));
            Tablero.Poner(NuevoDestructorEnLimites(r));
            Tablero.Poner(NuevoDestructorEnLimites(r));
            Tablero.Poner(NuevoBuqueEnLimites(r));
            Tablero.Poner(NuevoPortaavionesEnLimites(r));
            Tablero.Poner(NuevoRompehielosEnLimites(r));
        }

        public void AvanzarTurno()
        {
            if (this.Terminado)
            {
                return;
            }

            this.Puntaje -= CostoTurno;
            this.EvaluarPuntaje();
            this.Tablero.EvaluarColisiones();
            this.Tablero.MoverNaves();
            this.Tablero.ActualizarEstadoExplosivos();
        }

        // TODO restringir este método a la etapa posterior al inicio del juego
        public void Poner(Explosivo explosivo)
        {
            this.Puntaje -= explosivo.Costo;
            this.Tablero.Poner(explosivo);
        }

        public void EvaluarPuntaje()
        {
            if (this.Puntaje <= 0)
            {
                if (OnSinPuntosDisponibles != null)
                {
                    OnSinPuntosDisponibles.Invoke();
                }
            }
        }

        NaveBase NuevaLanchaEnLimites(Random r)
        {
            return NuevaNaveEnLimites(r, (pos, dir, mov) => new Lancha(pos, dir, mov), 2);
        }

        NaveBase NuevoDestructorEnLimites(Random r)
        {
            return NuevaNaveEnLimites(r, (pos, dir, mov) => new Destructor(pos, dir, mov), 3);
        }

        NaveBase NuevoBuqueEnLimites(Random r)
        {
            return NuevaNaveEnLimites(r, (pos, dir, mov) => new Buque(pos, dir, mov), 4);
        }

        NaveBase NuevoPortaavionesEnLimites(Random r)
        {
            return NuevaNaveEnLimites(r, (pos, dir, mov) => new Portaaviones(pos, dir, mov), 5);
        }

        NaveBase NuevoRompehielosEnLimites(Random r)
        {
            return NuevaNaveEnLimites(r, (pos, dir, mov) => new Rompehielos(pos, dir, mov), 3);
        }

        NaveBase NuevaNaveEnLimites(Random r, Func<Posicion, Posicion, Movimiento, NaveBase> constructor, int largoNave)
        {
            var mov = DireccionDeMovimientoRandom(r);
            var pos = PosicionRandom(r);

            Posicion dir;
            while (true)
            {
                var n = r.Next(direccionesPosibles.Count() - 1);
                dir = direccionesPosibles.Skip(n).First();
                if (Tablero.Limites.Contiene(Area.FromPosiciones(pos, pos + dir * largoNave)))
                {
                    return constructor(pos, dir, new Movimiento(Tablero.Limites, mov));
                }
            }
        }

        Posicion DireccionDeMovimientoRandom(Random r)
        {
            var n = r.Next(movimientosPosibles.Count() - 1);
            var mov = movimientosPosibles.Skip(n).First();
            return mov;
        }

        Posicion PosicionRandom(Random r)
        {
            var n = r.Next(Tablero.Limites.Posiciones.Count() - 1);
            var pos = Tablero.Limites.Posiciones.Skip(n).First();
            return pos;
        }
    }
}