namespace BatallaNavalgo.Modelo.Naves.Partes
{
    using BatallaNavalgo.Modelo.Explosivos;

    public class ParteDeRompehielos : ParteBase
    {
        public ParteDeRompehielos()
        {
        }

        public ParteDeRompehielos(Posicion offset)
            : base(offset)
        {
            this.RecibioPrimerDisparo = false;
        }

        public bool RecibioPrimerDisparo { get; set; }

        public override void Recibir(Explosivo explotable, Area area)
        {
            if (!RecibioPrimerDisparo)
            {
                if (area.Contiene(this.Posicion))
                {
                    RecibioPrimerDisparo = true;
                }
            }
            else
            {
                base.Recibir(explotable, area);
            }
        }
    }
}