namespace BatallaNavalgo.Modelo.Naves.Partes
{
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;

    public class ParteDeDestructor : ParteBase
    {
        public ParteDeDestructor()
        {
        }

        public ParteDeDestructor(Posicion offset)
            : base(offset)
        {
        }

        public virtual void Recibir(MinaSubmarinaBase explotable, Area area)
        {
        }
    }
}