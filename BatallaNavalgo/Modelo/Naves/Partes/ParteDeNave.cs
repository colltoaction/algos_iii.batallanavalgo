namespace BatallaNavalgo.Modelo.Naves.Partes
{
    public class ParteDeNave : ParteBase
    {
        public ParteDeNave()
        {
        }

        public ParteDeNave(Posicion offset)
            : base(offset)
        {
        }
    }
}