namespace BatallaNavalgo.Modelo.Naves.Partes
{
    using System;
    using System.Xml.Serialization;
    using BatallaNavalgo.Modelo.Explosivos;

    [Serializable]
    [XmlInclude(typeof(ParteDeDestructor))]
    [XmlInclude(typeof(ParteDeNave))]
    [XmlInclude(typeof(ParteDeRompehielos))]
    public abstract class ParteBase
    {
        public ParteBase()
        {
        }

        public ParteBase(Posicion pos)
        {
            this.Posicion = pos;
        }

        [XmlElement("Posicion")]
        public Posicion Posicion { get; set; }

        [XmlElement("Area")]
        public bool Destruida { get; set; }
        
        public virtual void Recibir(Explosivo explotable, Area area)
        {
            if (area.Contiene(this.Posicion))
            {
                Destruida = true;
            }
        }
        
        public override bool Equals(object obj)
        {
            return obj.GetType() == this.GetType();
        }
        
        public override int GetHashCode()
        {
            return this.GetType().GetHashCode();
        }
    }
}