﻿namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Partes;

    public class Buque : NaveBase
    {
        public Buque()
        {
        }   

        public Buque(Posicion posicion, Posicion direccion, Movimiento mov)
            : base(mov, new ParteDeNave(posicion), new ParteDeNave(posicion + direccion), new ParteDeNave(posicion + (direccion * 2)), new ParteDeNave(posicion + (direccion * 3)))
        {
        }

        public override bool Destruida
        {
            get
            {
                return Partes.Any(parte => parte.Destruida);
            }   
        }
    }
}
