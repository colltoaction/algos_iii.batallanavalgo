﻿namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Partes;

    public class Rompehielos : NaveBase
    {
        public Rompehielos()
        {
        }

        public Rompehielos(Posicion posicion, Posicion direccion, Movimiento mov)
            : base(mov, new ParteDeRompehielos(posicion), new ParteDeRompehielos(posicion + direccion), new ParteDeRompehielos(posicion + direccion * 2))
        {
        }
    }
}
