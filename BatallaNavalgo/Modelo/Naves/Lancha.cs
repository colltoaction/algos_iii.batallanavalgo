namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Partes;

    public class Lancha : NaveBase
    {
        public Lancha()
        {
        }

        public Lancha(Posicion posicion, Posicion direccion, Movimiento mov)
            : base(mov, new ParteDeNave(posicion), new ParteDeNave(posicion + direccion))
        {
        }
    }
}