namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Xml.Serialization;
    using Explosivos;
    using Partes;

    [Serializable]
    [XmlRootAttribute("Nave")]
    [XmlInclude(typeof(Buque))]
    [XmlInclude(typeof(Destructor))]
    [XmlInclude(typeof(Lancha))]
    [XmlInclude(typeof(Portaaviones))]
    [XmlInclude(typeof(Rompehielos))]
    public abstract class NaveBase
    {
        Movimiento mov;

        protected NaveBase()
        {
        }

        protected NaveBase(Movimiento mov, params ParteBase[] partes)
        {
            if (partes.Length == 0)
            {
                throw new ArgumentException();
            }

            this.Partes = partes;
            this.mov = mov;
        }
        
        // para serialización
        public Movimiento Movimiento
        {
            get { return mov; }
            set { mov = value; }
        }

        [XmlArrayItem("Parte")]
        public ParteBase[] Partes { get; set; }

        public virtual bool Destruida
        {
            get
            {
                return Partes.All(parte => parte.Destruida);
            }

            set
            {
            }
        }

        public Area Area
        {
            get
            {
                return new Area(Partes.Select(p => p.Posicion).ToList<Posicion>());
            }

            set
            {
            }
        }

        public void Recibir(Explosivo explosivo, Area area)
        {
            foreach (dynamic parte in Partes)
            {
                parte.Recibir((dynamic)explosivo, (dynamic)area);
            }
        }

        public void Mover()
        {
            // cacheo el área para que no se actualice mientras muevo las piezas
            var areaActual = Area;
            foreach (var parte in Partes)
            {
                parte.Posicion = mov.Siguiente(areaActual, parte.Posicion);
            }
        }
    }
}