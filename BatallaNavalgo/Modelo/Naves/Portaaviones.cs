﻿namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Partes;

    public class Portaaviones : NaveBase
    {
        public Portaaviones()
        {
        }

        public Portaaviones(Posicion posicion, Posicion direccion, Movimiento mov)
            : base(mov, new ParteDeNave(posicion), new ParteDeNave(posicion + direccion), new ParteDeNave(posicion + direccion * 2), new ParteDeNave(posicion + direccion * 3), new ParteDeNave(posicion + direccion * 4))
        {
        }
    }
}
