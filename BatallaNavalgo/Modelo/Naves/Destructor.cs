﻿namespace BatallaNavalgo.Modelo.Naves
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using Partes;

    public class Destructor : NaveBase
    {
        public Destructor()
        {
        }

        public Destructor(Posicion posicion, Posicion direccion, Movimiento mov)
            : base(mov, new ParteDeDestructor(posicion), new ParteDeDestructor(posicion + direccion), new ParteDeDestructor(posicion + direccion * 2))
        {
        }

        public void Recibir(Disparo disparo, Area area)
        {
            foreach (var parte in Partes)
            {
                parte.Recibir(disparo, area);
            }
        }

        public void Recibir(MinaSubmarinaBase explosivo, Area area)
        {
        }
    }
}