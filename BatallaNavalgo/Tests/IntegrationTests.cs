namespace BatallaNavalgo.Tests
{
    using System.Linq;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves;
    using NUnit.Framework;
    using System.IO;

    [TestFixture]
    public class IntegrationTests
    {
        [Test]
        public void DestruyoLanchaConDosDisparosEnDosTurnosYTerminoJuego()
        {
            var juego = new Juego();
            bool eventRaised = false;
            juego.OnGanado += () => eventRaised = true;
            var tablero = juego.Tablero;
            var lancha = new Lancha(new Posicion(4, 5), Posicion.NorEste, new Movimiento(tablero.Limites, Posicion.NorEste));

            tablero.Poner(lancha);

            Assert.That(juego.Puntaje, Is.EqualTo(10000));

            juego.Poner(new Disparo(new Posicion(4, 5))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(9790));

            juego.Poner(new Disparo(new Posicion(6, 7))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.True);
            Assert.That(juego.Terminado, Is.True);
            Assert.That(juego.Puntaje, Is.EqualTo(9580));
        }

        [Test]
        public void DestruyoLanchaYRompehielos()
        {
            var juego = new Juego();
            bool eventRaised = false;
            juego.OnGanado += () => eventRaised = true;
            var tablero = juego.Tablero;
            var puntaje = juego.Puntaje;
            var lancha = new Lancha(new Posicion(8, 3), Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            var rompehielos = new Rompehielos(new Posicion(0, 0), Posicion.Norte, new Movimiento(tablero.Limites, Posicion.Norte));

            tablero.Poner(lancha);
            tablero.Poner(rompehielos);
            
            Assert.That(juego.Puntaje, Is.EqualTo(10000));

            // en cuatro turnos destruyo el rompehielos
            juego.Poner(new MinaTripleConRetardo(new Posicion(0, 4))); // 125 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(9865));

            // en tres turnos destruyo el rompehielos
            juego.Poner(new MinaTripleConRetardo(new Posicion(0, 5))); // 125 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(9730));

            // exploto parte izquierda de lancha
            juego.Poner(new Disparo(new Posicion(6, 3))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(9520));

            // fallo un disparo
            juego.Poner(new Disparo(new Posicion(4, 4))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(9310));

            // exploto parte derecha de lancha y mina destruye rompehielos
            juego.Poner(new Disparo(new Posicion(5, 3))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.True);
            Assert.That(juego.Terminado, Is.True);
            Assert.That(juego.Puntaje, Is.EqualTo(9100));
        }

        [Test]
        public void NoDestruyoDestructorPartidaTerminadaPorPuntos()
        {
            var juego = new Juego(500);
            bool eventRaised = false;
            juego.OnSinPuntosDisponibles += () => eventRaised = true;
            var tablero = juego.Tablero;
            var puntaje = juego.Puntaje;
            var lancha = new Lancha(new Posicion(5, 5), Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            var destructor = new Destructor(new Posicion(0, 0), Posicion.Norte, new Movimiento(tablero.Limites, Posicion.Norte));

            tablero.Poner(lancha);
            tablero.Poner(destructor);

            Assert.That(juego.Puntaje, Is.EqualTo(500));

            // en cuatro turnos destruyo la lancha
            juego.Poner(new MinaTripleConRetardo(new Posicion(7, 5))); // 125 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(365));

            // disparo al destructor
            juego.Poner(new Disparo(new Posicion(2, 0))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
            Assert.That(juego.Puntaje, Is.EqualTo(155));

            // disparo al destructor y me quedo sin puntos
            juego.Poner(new Disparo(new Posicion(4, 0))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            
            Assert.That(eventRaised, Is.True);
            Assert.That(juego.Terminado, Is.True);
        }

        [Test]
        public void PartidaGeneradaTieneLasNavesEsperadas()
        {
            var juego = new Juego();
            var tablero = juego.Tablero;
            juego.ColocarNaves();
            
            Assert.That(tablero.Naves.OfType<Lancha>().Count(), Is.EqualTo(2));
            Assert.That(tablero.Naves.OfType<Destructor>().Count(), Is.EqualTo(2));
            Assert.That(tablero.Naves.OfType<Buque>().Count(), Is.EqualTo(1));
            Assert.That(tablero.Naves.OfType<Portaaviones>().Count(), Is.EqualTo(1));
            Assert.That(tablero.Naves.OfType<Rompehielos>().Count(), Is.EqualTo(1));
        }

        [Test]
        public void SerializarJuego()
        {
            var juego = new Juego(5000);
            bool eventRaised = false;
            juego.OnSinPuntosDisponibles += () => eventRaised = true;
            var tablero = juego.Tablero;
            var puntaje = juego.Puntaje;
            var lancha = new Lancha(new Posicion(5, 5), Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            var destructor = new Buque(new Posicion(0, 0), Posicion.Norte, new Movimiento(tablero.Limites, Posicion.Norte));

            tablero.Poner(lancha);
            tablero.Poner(destructor);
            
            // en cuatro turnos destruyo la lancha
            juego.Poner(new MinaTripleConRetardo(new Posicion(7, 5))); // 125 puntos
            juego.AvanzarTurno(); // 10 puntos

            // disparo al agua dos veces
            juego.Poner(new Disparo(new Posicion(1, 1))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            juego.Poner(new Disparo(new Posicion(1, 1))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos

            // pongo una mina por contacto
            juego.Poner(new MinaPorContacto(new Posicion(3, 1))); // 150 puntos
            juego.AvanzarTurno(); // 10 puntos

            // guardo el estado del juego
            Assert.That(Serializador.SerializeJuego(juego, "juego.xml"), Is.True);
            Assert.That(File.Exists("juego.xml"), Is.True);
            Assert.That(eventRaised, Is.False);
            Assert.That(juego.Terminado, Is.False);
        }

        [Test]
        public void DeserializarJuego()
        {
            var juego = new Juego(5000);
            var tablero = juego.Tablero;
            var puntaje = juego.Puntaje;
            var lancha = new Lancha(new Posicion(5, 5), Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            var destructor = new Buque(new Posicion(0, 0), Posicion.Norte, new Movimiento(tablero.Limites, Posicion.Norte));

            tablero.Poner(lancha);
            tablero.Poner(destructor);

            // en cuatro turnos destruyo la lancha
            juego.Poner(new MinaTripleConRetardo(new Posicion(7, 5))); // 125 puntos
            juego.AvanzarTurno(); // 10 puntos

            // disparo al agua dos veces
            juego.Poner(new Disparo(new Posicion(1, 1))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos
            juego.Poner(new Disparo(new Posicion(1, 1))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos

            // pongo una mina por contacto
            juego.Poner(new MinaPorContacto(new Posicion(3, 1))); // 150 puntos

            // borro el contenido actual.
            juego = new Juego();
            tablero = juego.Tablero;

            // cargo el estado guardado
            juego = Serializador.DeserializarDeXml("juego_guardado.xml");
            tablero = juego.Tablero;

            Assert.That(juego.Tablero.Naves.Count(), Is.EqualTo(2));
            Assert.That(juego.Tablero.Naves.Count(p => p.Destruida), Is.EqualTo(1));
            Assert.That(juego.Puntaje, Is.EqualTo(4285));
            Assert.That(juego.Terminado, Is.False);

            // destruyo el buque que quedaba
            juego.Poner(new Disparo(new Posicion(0, 6))); // 200 puntos
            juego.AvanzarTurno(); // 10 puntos

            foreach (var nave in juego.Tablero.Naves)
            {
                Assert.That(nave.Partes.Count(p => p.Destruida), Is.GreaterThan(0));
            }

            Assert.That(tablero.Naves.Count(p => p.Destruida), Is.EqualTo(2));
            Assert.That(tablero.Naves.Count(), Is.EqualTo(2));
            Assert.That(juego.Puntaje, Is.EqualTo(4075));
            Assert.That(juego.Terminado, Is.True);
        }
    }
}