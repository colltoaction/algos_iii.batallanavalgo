namespace BatallaNavalgo.Tests
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;

    [TestFixture]
    public class AreaTests
    {
        [Test]
        public void AreasUnitariasQueSonIgualesIntersecan()
        {
            var area1 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreasUnitariasQueSonDistintasNoIntersecan()
        {
            var area1 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.Este, Posicion.Este);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void AreaDeSpan2IntersecaUnitaria()
        {
            var area1 = Area.FromPosiciones(Posicion.Este, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreaDeSpan3IntersecaUnitaria()
        {
            var area1 = Area.FromPosiciones(Posicion.Oeste, Posicion.Este);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreaUnitariaIntersecaDeSpan3()
        {
            var area1 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.Oeste, Posicion.Este);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreasIntersecanEnLadoLateral()
        {
            var area1 = Area.FromPosiciones(new Posicion(-100, 30), new Posicion(30, 50));
            var area2 = Area.FromPosiciones(new Posicion(30, 38), new Posicion(42, 0));

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreasIntersecanEnLadoSuperior()
        {
            var area1 = Area.FromPosiciones(new Posicion(15, 30), new Posicion(30, 12));
            var area2 = Area.FromPosiciones(new Posicion(17, 12), new Posicion(18, 5));

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreasIntersecanEnLadoInferior()
        {
            var area1 = Area.FromPosiciones(new Posicion(17, 12), new Posicion(18, 5));
            var area2 = Area.FromPosiciones(new Posicion(15, 30), new Posicion(30, 12));

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreasIntersecanEnEsquina()
        {
            var area1 = Area.FromPosiciones(new Posicion(42, 5), new Posicion(43, 4));
            var area2 = Area.FromPosiciones(new Posicion(41, 5), new Posicion(42, 6));

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void NoIntersecaAreaDiagonalEnEsquinaComoSiFueraRectangular()
        {
            var area1 = Area.FromPosiciones(new Posicion(5, 2), new Posicion(20, 0));
            var area2 = Area.FromPosicionesDiagonales(Posicion.Cero, new Posicion(10, 10));

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void IntersecaAreaDiagonalQueLoAtraviesa()
        {
            var area1 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var area2 = Area.FromPosicionesDiagonales(new Posicion(-10, -10), new Posicion(10, 10));

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void NoIntersecaDiagonalApuntandoAlNorOeste()
        {
            var area1 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.NorOeste * 10);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void NoIntersecaDiagonalApuntandoAlSurOeste()
        {
            var area1 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurOeste * 10);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void NoIntersecaDiagonalApuntandoAlSurEste()
        {
            var area1 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurEste * 10);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void AreasIgualesOcupanLosMismosTiles()
        {
            var area1 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);

            Assert.That(area1, Is.EqualTo(area2));
        }

        [Test]
        public void AreasIntersecadasNoSonIguales()
        {
            var area1 = Area.FromPosiciones(Posicion.Oeste, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Este);

            Assert.That(area1, Is.Not.EqualTo(area2));
        }

        [Test]
        public void NoEsIgualAAreaContenida()
        {
            var area1 = Area.FromPosiciones(Posicion.SurEste, Posicion.NorOeste);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1, Is.Not.EqualTo(area2));
        }

        [Test]
        public void NoEsIgualAAreaDiagonal()
        {
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.SurOeste, Posicion.NorEste);

            Assert.That(area1, Is.Not.EqualTo(area2));
        }

        [Test]
        public void DesplazadaEsIgualACadaComponenteDesplazada()
        {
            var pos = Posicion.Sur;
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.SurOeste + pos, Posicion.NorEste + pos);

            Assert.That(area1 + pos, Is.EqualTo(area2));
        }

        [Test]
        public void DesplazoDosAreasYSonIguales()
        {
            var area1 = Area.FromPosiciones(Posicion.Oeste, new Posicion(4, 5));
            var area2 = Area.FromPosiciones(Posicion.Este, new Posicion(6, 5));

            Assert.That(area1 + Posicion.Este, Is.EqualTo(area2 + Posicion.Oeste));
        }

        [Test]
        public void IntersecaOtraDiagonal()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurEste * 10);
            var area2 = Area.FromPosicionesDiagonales(Posicion.Sur * 10, Posicion.Este * 10);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreaDiagonalIntersecaEnEsquina()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurEste);
            var area2 = Area.FromPosiciones(Posicion.NorOeste, Posicion.Cero);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void AreaDiagonalNoIntersecaEnEsquinaComoSiFueraRectangular()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.NorEste * 10);
            var area2 = Area.FromPosiciones(new Posicion(5, 2), new Posicion(20, 0));

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void AreaDiagonalIntersecaRectanguloAtravesado()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.SurEste * 10 , Posicion.NorOeste * 10);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1.Interseca(area2), Is.True);
        }

        [Test]
        public void NoIntersecaRectanguloEnDiagonalNorOeste()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.NorOeste * 10);
            var area2 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void NoIntersecaRectanguloEnDiagonalSurOeste()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurOeste * 10);
            var area2 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void NoIntersecaRectanguloEnDiagonalSurEste()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Cero, Posicion.SurEste * 10);
            var area2 = Area.FromPosiciones(Posicion.NorEste, Posicion.NorEste);

            Assert.That(area1.Interseca(area2), Is.False);
        }

        [Test]
        public void AreasDiagonalesIgualesOcupanLosMismosTiles()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.NorOeste, Posicion.SurEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.SurEste, Posicion.NorOeste);

            Assert.That(area1, Is.EqualTo(area2));
        }

        [Test]
        public void NoEsIgualAAreaDiagonalContenida()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.NorOeste, Posicion.SurEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.SurEste, Posicion.NorOeste * 2);

            Assert.That(area1, Is.Not.EqualTo(area2));
        }

        [Test]
        public void NoEsIgualAAreaRectangular()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);

            Assert.That(area1, Is.Not.EqualTo(area2));
        }

        [Test]
        public void DiagonalDesplazadaEsIgualACadaComponenteDesplazada()
        {
            var pos = Posicion.Sur;
            var area1 = Area.FromPosicionesDiagonales(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosicionesDiagonales(Posicion.SurOeste + pos, Posicion.NorEste + pos);

            Assert.That(area1 + pos, Is.EqualTo(area2));
        }

        [Test]
        public void DesplazoDosAreasDiagonalesYSonIguales()
        {
            var area1 = Area.FromPosicionesDiagonales(Posicion.Oeste, new Posicion(4, 5));
            var area2 = Area.FromPosicionesDiagonales(Posicion.Este, new Posicion(6, 5));

            Assert.That(area1 + Posicion.Este, Is.EqualTo(area2 + Posicion.Oeste));
        }

        [Test]
        public void ContieneAreaMasChica()
        {
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);

            Assert.That(area1.Contiene(area2), Is.True);
        }

        [Test]
        public void NoContieneAreaMasGrande()
        {
            var area1 = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var area2 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);

            Assert.That(area1.Contiene(area2), Is.False);
        }

        [Test]
        public void NoContieneAreaMasChicaAfuera()
        {
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.Este * 2, Posicion.Este * 2);

            Assert.That(area1.Contiene(area2), Is.False);
        }

        [Test]
        public void NoContieneAreaIntersecante()
        {
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.Este, Posicion.Este * 2);

            Assert.That(area1.Contiene(area2), Is.False);
        }

        [Test]
        public void NoContieneAreaQueLaAtraviesa()
        {
            var area1 = Area.FromPosiciones(Posicion.SurOeste, Posicion.NorEste);
            var area2 = Area.FromPosiciones(Posicion.Oeste * 2, Posicion.Este * 2);

            Assert.That(area1.Contiene(area2), Is.False);
        }
    }
}