namespace BatallaNavalgo.Tests
{
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;

    [TestFixture]
    public class MovimientoTests
    {
        [Test]
        public void SiguienteConAreaOcupada()
        {
            var limites = Area.FromPosiciones(Posicion.NorEste, Posicion.SurOeste);
            var ocupada = Area.FromPosiciones(Posicion.Cero, Posicion.Cero);
            var mov = new Movimiento(limites, Posicion.Este);

            Assert.That(mov.Siguiente(ocupada, Posicion.Oeste), Is.EqualTo(Posicion.Cero));
        }

        [Test]
        public void SiguienteConAreaOcupadaRebota()
        {
            var limites = Area.FromPosiciones(Posicion.NorEste, Posicion.SurOeste);
            var ocupada = Area.FromPosiciones(Posicion.NorEste, Posicion.Cero);
            var mov = new Movimiento(limites, Posicion.Este);

            Assert.That(mov.Siguiente(ocupada, Posicion.Cero), Is.EqualTo(Posicion.Oeste));
        }

        [Test]
        public void ReboteEnDiagonalEsA90Grados()
        {
            var limites = Area.FromPosiciones(Posicion.NorEste, Posicion.SurOeste);
            var ocupada = Area.FromPosiciones(Posicion.Este, Posicion.Este);
            var mov = new Movimiento(limites, Posicion.NorEste);

            Assert.That(mov.Siguiente(ocupada, Posicion.Este), Is.EqualTo(Posicion.Norte));
        }

        Area AreaGrande()
        {
            return Area.FromPosiciones(Posicion.SurOeste * 100, Posicion.NorEste * 100);
        }
    }
}