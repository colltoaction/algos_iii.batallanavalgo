namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;

    [TestFixture]
    public class LanchaTests : NaveTests
    {
        protected override NaveBase NewNave(Posicion pos, Movimiento mov)
        {
            return new Lancha(pos, Posicion.Este, mov);
        }

        [Test]
        public void LuegoDeDestruirUnaParteDeUnaNaveNoEstaDestruida()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDestruirAmbasParteDeUnaNaveEstaDestruida()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);

            nave.Recibir(disparo, disparo.Area);
            nave.Recibir(disparo2, disparo2.Area);

            Assert.That(nave.Destruida, Is.True);
        }

        [Test]
        public void SiFallaElDisparoLaNaveNoEstaDestruida()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(Posicion.Cero);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void EsDestruibleConMinasSubmarinas()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var mina1 = new MinaPorContacto(pos);
            var mina2 = new MinaPorContacto(pos+Posicion.Este);

            mina1.ExplotarEn(nave);
            mina2.ExplotarEn(nave);

            Assert.That(nave.Destruida, Is.True);
        }
    }
}