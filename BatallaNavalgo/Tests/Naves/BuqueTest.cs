﻿namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;

    [TestFixture]
    public class BuqueTest : NaveTests
    {
        protected override NaveBase NewNave(Posicion pos, Movimiento mov)
        {
            return new Buque(pos, Posicion.Este, mov);
        }

        [Test]
        public void LuegoDeDestruirUnaParteEstaDestruido()
        {
            var pos = new Posicion(3, 2);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.True);
        }
        
        [Test]
        public void BuqueNoDestruidoSiFallaElDisparo()
        {
            var pos = new Posicion(4, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(Posicion.Cero);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }
    }
}
