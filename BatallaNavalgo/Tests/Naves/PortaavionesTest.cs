﻿namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;

    [TestFixture]
    public class PortaavionesTest : NaveTests
    {
        protected override NaveBase NewNave(Posicion pos, Movimiento mov)
        {
            return new Portaaviones(pos, Posicion.Este, mov);
        }

        [Test]
        public void LuegoDeDestruirUnaParteDeUnPortaavionesNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDestruirTodasLasParteDeUnPortaavionesEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));
            var disparo4 = new Disparo(pos + (Posicion.Este * 3));
            var disparo5 = new Disparo(pos + (Posicion.Este * 4));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo3, disparo3.Area);
            nave.Recibir(disparo4, disparo4.Area);
            nave.Recibir(disparo5, disparo5.Area);

            Assert.That(nave.Destruida, Is.True);
        }

        [Test]
        public void SiFallaElDisparoElPortaavionesNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);
            var disparoCero = new Disparo(Posicion.Cero);

            nave.Recibir(disparo, disparo.Area);
            nave.Recibir(disparoCero, disparoCero.Area);

            Assert.That(nave.Destruida, Is.False);
        }
    }
}
