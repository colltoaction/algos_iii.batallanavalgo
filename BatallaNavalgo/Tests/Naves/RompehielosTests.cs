﻿namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;

    [TestFixture]
    public class RompehielosTests : NaveTests
    {
        protected override NaveBase NewNave(Posicion pos, Movimiento mov)
        {
            return new Rompehielos(pos, Posicion.Este, mov);
        }

        [Test]
        public void NuevoRompehielosNoEstaDestruido()
        {
            var nave = NewNave(new Posicion(4, 2), null);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDestruirUnaParteDeUnRompehielosNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDispararATodasLasParteDeUnRompehielosNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo3, disparo3.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void MientrasNoTodasLasPartesRecibanDosDisparosNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo3, disparo3.Area);
            nave.Recibir(disparo3, disparo3.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void MientrasNoTodasLasPartesRecibanDosDisparosNoEstaDestruido2()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo3, disparo3.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo1, disparo1.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDispararDosVecesATodasLasParteDeUnRompehielosEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo3, disparo3.Area);
            nave.Recibir(disparo3, disparo3.Area);

            Assert.That(nave.Destruida, Is.True);
        }

        [Test]
        public void SiFallaElDisparoElRompehielosNoEstaDestruido()
        {
            var pos = new Posicion(3, 5);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(Posicion.Cero);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }
    }
}
