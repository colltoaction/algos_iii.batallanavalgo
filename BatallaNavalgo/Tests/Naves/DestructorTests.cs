﻿namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;

    [TestFixture]
    public class DestructorTests : NaveTests
    {
        protected override NaveBase NewNave(Posicion pos, Movimiento mov)
        {
            return new Destructor(pos, Posicion.Este, mov);
        }

        [Test]
        public void SiFallaElDisparoNoEstaDestruido()
        {
            var nave = NewNave(new Posicion(2, 2), null);
            var pos = new Posicion(3, 3);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }
        
        [Test]
        public void LuegoDeDisparAUnaParteNoEstaDestruido()
        {
            var pos = new Posicion(2, 2);
            var nave = NewNave(pos, null);
            var disparo = new Disparo(pos);

            nave.Recibir(disparo, disparo.Area);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void LuegoDeDestruirTodasLasPartesEstaDestruido()
        {
            var pos = new Posicion(2, 2);
            var nave = NewNave(pos, null);
            var disparo1 = new Disparo(pos);
            var disparo2 = new Disparo(pos + Posicion.Este);
            var disparo3 = new Disparo(pos + (Posicion.Este * 2));

            nave.Recibir(disparo1, disparo1.Area);
            nave.Recibir(disparo2, disparo2.Area);
            nave.Recibir(disparo3, disparo3.Area);

            Assert.That(nave.Destruida, Is.True);
        }

        [Test]
        public void NoEstaDestruidoSiChocaConMinasSubmarinas()
        {
            var pos1 = new Posicion(2, 2);
            var pos2 = new Posicion(3, 2);
            var pos3 = new Posicion(4, 2);
            var explosivo1 = new MinaPorContacto(pos1);
            var explosivo2 = new MinaPorContacto(pos2);
            var explosivo3 = new MinaPorContacto(pos3);
            var nave = NewNave(pos1, null);

            nave.Recibir(explosivo1, explosivo1.Area);
            nave.Recibir(explosivo2, explosivo2.Area);
            nave.Recibir(explosivo3, explosivo3.Area);

            Assert.That(nave.Destruida, Is.False);

        }
    }
}