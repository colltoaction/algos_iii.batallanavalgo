namespace BatallaNavalgo.Tests.Naves
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Naves;
    using BatallaNavalgo.Modelo.Explosivos;

    public abstract class NaveTests
    {
        protected abstract NaveBase NewNave(Posicion pos, Movimiento mov);

        [Test]
        public void NuevaNaveNoEstaDestruida()
        {
            var nave = NewNave(new Posicion(4,2), null);

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void NaveSeMueveUnTile()
        {
            var dir = Posicion.NorEste;
            var nave = NewNave(new Posicion(4,2), new Movimiento(AreaGrande(), dir));
            var area = nave.Area;

            nave.Mover();

            Assert.That(nave.Area, Is.EqualTo(area + dir));
        }

        [Test]
        public void NaveSeMueveDosTiles()
        {
            var dir = Posicion.Sur;
            var nave = NewNave(new Posicion(4,2), new Movimiento(AreaGrande(), dir));
            var area = nave.Area;
            
            nave.Mover();
            nave.Mover();

            Assert.That(nave.Area, Is.EqualTo(area + (dir * 2)));
        }

        Area AreaGrande()
        {
            return Area.FromPosiciones(Posicion.SurOeste * 100, Posicion.NorEste * 100);
        }
    }
}