namespace BatallaNavalgo.Tests.Partes
{
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves.Partes;
    using NUnit.Framework;

    [TestFixture]
    public class ParteDeNaveTests
    {
        [Test]
        public void NuevaParteNoEstaDestruida()
        {
            var parteDeNave = new ParteDeNave(Posicion.Cero);
            
            Assert.That(parteDeNave.Destruida, Is.False);
        }
        
        [Test]
        public void ParteLuegoDeRecibirDisparoEstaDestruida()
        {
            var parte = new ParteDeNave(Posicion.Cero);
            var disparo = new Disparo(Posicion.Cero);
            
            parte.Recibir(disparo, disparo.Area);
            
            Assert.That(parte.Destruida, Is.True);
        }
        
        [Test]
        public void ParteNoEstaDestruidaSiNoRecibeEnSuPosicion()
        {
            var parte = new ParteDeNave(new Posicion(4, 5));
            var disparo = new Disparo(Posicion.Cero);
            
            parte.Recibir(disparo, disparo.Area);
            
            Assert.That(parte.Destruida, Is.False);
        }
        
        [Test]
        public void ParteLuegoDeRecibirExplosionSubmarinaEstaDestruida()
        {
            var parte = new ParteDeNave(Posicion.Cero);
            var explosivo = new MinaPorContacto(Posicion.Cero);
            
            parte.Recibir(explosivo, explosivo.Area);
            
            Assert.That(parte.Destruida, Is.True);
        }
    }
}