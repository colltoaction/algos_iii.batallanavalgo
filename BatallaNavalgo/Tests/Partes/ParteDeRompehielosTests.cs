namespace BatallaNavalgo.Tests.Partes
{
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves.Partes;
    using NUnit.Framework;
    
    [TestFixture]
    public class ParteDeRompehielosTests
    {
        [Test]
        public void NuevaParteNoEstaDestruida()
        {
            var parte = new ParteDeRompehielos(Posicion.Cero);
            
            Assert.That(parte.Destruida, Is.False);
        }
        
        [Test]
        public void ParteLuegoDeRecibirDisparoNoEstaDestruida()
        {
            var parte = new ParteDeRompehielos(Posicion.Cero);
            var disparo = new Disparo(Posicion.Cero);
            
            parte.Recibir(disparo, disparo.Area);
            
            Assert.That(parte.Destruida, Is.False);
        }

        [Test]
        public void ParteLuegoDeRecibirDosDisparoEstaDestruida()
        {
            var parte = new ParteDeRompehielos(Posicion.Cero);
            var disparo = new Disparo(Posicion.Cero);

            parte.Recibir(disparo, disparo.Area);
            parte.Recibir(disparo, disparo.Area);

            Assert.That(parte.Destruida, Is.True);
        }

        [Test]
        public void ParteNoEstaDestruidaSiNoRecibeDisparosEnSuPosicion()
        {
            var parte = new ParteDeRompehielos(new Posicion(4, 5));
            var disparo = new Disparo(Posicion.Cero);

            parte.Recibir(disparo, disparo.Area);
            parte.Recibir(disparo, disparo.Area);

            Assert.That(parte.Destruida, Is.False);
        }

        [Test]
        public void ParteNoEstaDestruidaSiNoRecibeDisparosEnSuPosicion2()
        {
            var posicion = new Posicion(4, 5);
            var parte = new ParteDeRompehielos(posicion);
            var disparo1 = new Disparo(Posicion.Cero);
            var disparo2 = new Disparo(posicion);

            parte.Recibir(disparo1, disparo1.Area);
            parte.Recibir(disparo2, disparo2.Area);

            Assert.That(parte.Destruida, Is.False);
        }        
    }
}