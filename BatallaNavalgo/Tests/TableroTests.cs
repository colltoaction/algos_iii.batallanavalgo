namespace BatallaNavalgo.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Naves;

    [TestFixture]
    public class TableroTests
    {
        [Test]
        public void MoverNavesActualizaPosicionesDeNaves()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este, Posicion.Norte, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);

            tablero.MoverNaves();

            Assert.That(nave.Area, Is.EqualTo(Area.FromPosiciones(Posicion.Este * 2, Posicion.Este * 2 + Posicion.Norte)));
        }

        [Test]
        public void MoverNaveParalelaAlBordeNoLaHaceRebotar()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 8, Posicion.Oeste, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);

            tablero.MoverNaves();

            Assert.That(nave.Area, Is.EqualTo(Area.FromPosiciones(Posicion.Este * 8, Posicion.Este * 9)));
        }

        [Test]
        public void MoverNavesEnBordeLasHaceRebotar()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 9, Posicion.Oeste, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);

            tablero.MoverNaves();

            Assert.That(nave.Area, Is.EqualTo(Area.FromPosiciones(Posicion.Este * 7, Posicion.Este * 8)));
        }

        [Test]
        public void EvaluarColisionesExplotaNavesAfectadas()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 4, Posicion.Oeste, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);
            tablero.Poner(new Disparo(Posicion.Este * 4));
            tablero.Poner(new Disparo(Posicion.Este * 3));

            tablero.EvaluarColisiones();

            Assert.That(nave.Destruida, Is.True);
        }

        [Test]
        public void EvaluarColisionesNoExplotaNavesNoAfectadas()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.NorEste, Posicion.NorEste, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);
            tablero.Poner(new Disparo(Posicion.Este * 4));
            tablero.Poner(new Disparo(Posicion.Este * 5));

            tablero.EvaluarColisiones();

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void ActualizarEstadoExplosivosDejaExplotadosLosDisparos()
        {
            var tablero = new Tablero();
            var disparo = new Disparo(Posicion.Este * 4);
            tablero.Poner(disparo);

            tablero.ActualizarEstadoExplosivos();

            Assert.That(disparo.Estado.Explotado, Is.True);
        }

        [Test]
        public void AlDestruirUnicaNaveNotificaTodasDestruidas()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 4, Posicion.NorOeste, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);
            tablero.Poner(new Disparo(Posicion.Este * 4));
            tablero.Poner(new Disparo(Posicion.Este * 3 + Posicion.Norte));
            bool eventRaised = false;
            tablero.OnTodasLasNavesDestruidas += () => eventRaised = true;

            tablero.EvaluarColisiones();

            Assert.That(eventRaised, Is.True);
        }

        [Test]
        public void AlDestruirUnaDeDosNavesNoNotificaTodasDestruidas()
        {
            var tablero = new Tablero();
            var naveNoAfectada = new Lancha(Posicion.NorEste, Posicion.Oeste, new Movimiento(tablero.Limites, Posicion.Este));
            var naveAfectada = new Lancha(Posicion.Este * 4, Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(naveNoAfectada);
            tablero.Poner(naveAfectada);
            tablero.Poner(new Disparo(Posicion.Este * 4));
            tablero.Poner(new Disparo(Posicion.Este * 5));
            bool eventRaised = false;
            tablero.OnTodasLasNavesDestruidas += () => eventRaised = true;

            tablero.EvaluarColisiones();

            Assert.That(eventRaised, Is.False);
        }

        [Test]
        public void UnExplosivoNoExplotaDosVeces()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 4, Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);
            tablero.Poner(new Disparo(Posicion.Este * 5));

            tablero.EvaluarColisiones();
            tablero.MoverNaves();
            tablero.ActualizarEstadoExplosivos();

            tablero.EvaluarColisiones();
            tablero.MoverNaves();
            tablero.ActualizarEstadoExplosivos();

            Assert.That(nave.Destruida, Is.False);
        }

        [Test]
        public void AlExplotarSeRemuevenLosExplosivosDeSuColeccion()
        {
            var tablero = new Tablero();
            var nave = new Lancha(Posicion.Este * 4, Posicion.Este, new Movimiento(tablero.Limites, Posicion.Este));
            tablero.Poner(nave);
            tablero.Poner(new Disparo(Posicion.Este * 5));
            tablero.Poner(new Disparo(Posicion.Norte * 2));
            tablero.Poner(new Disparo(Posicion.Cero));
            tablero.Poner(new Disparo(Posicion.Norte * 2));

            tablero.EvaluarColisiones();
            tablero.MoverNaves();
            tablero.ActualizarEstadoExplosivos();

            Assert.That(tablero.Explosivos.Count(), Is.EqualTo(0));
        }
    }
}

