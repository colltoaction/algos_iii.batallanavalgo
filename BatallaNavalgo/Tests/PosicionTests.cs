namespace BatallaNavalgo.Tests
{
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;

    [TestFixture]
    public class PosicionTests
    {
        [Test]
        public void PosicionesNoEsIgualANull()
        {
            Assert.That(new Posicion(0, 2), Is.Not.EqualTo(null));
        }

        [Test]
        public void PosicionNulaEsDistintaDeNullEsFalse()
        {
            Posicion posicion = null;
            Assert.That(posicion != null, Is.False);
        }

        [Test]
        public void MultiplicarPosicionPorEntero()
        {
           Assert.That(new Posicion(1,0)*3, Is.EqualTo(new Posicion(3,0)));
        }

        [Test]
        public void PosicionesConIgualesCoordenadasSonIguales()
        {
            Assert.That(new Posicion(1, 7), Is.EqualTo(new Posicion(1, 7)));
        }
        
        [Test]
        public void PosicionesConCoordenadasDistintasNoSonIguales()
        {
            Assert.That(new Posicion(6, 9), Is.Not.EqualTo(new Posicion(3, 1)));
        }

        [Test]
        public void SumarDerechaAPosicion()
        {
            Assert.That(new Posicion(2,0) + Posicion.Este, Is.EqualTo(new Posicion(3, 0)));
        }

        [Test]
        public void SumarIzquierdaAPosicion()
        {
            Assert.That(new Posicion(2, 0) + Posicion.Oeste, Is.EqualTo(new Posicion(1, 0)));
        }

        [Test]
        public void PosicionesIgualesEstanA45Grados()
        {
            Assert.That(Posicion.Este.A45Grados(Posicion.Este), Is.True);
        }

        [Test]
        public void PosicionesSurOesteYNorEsteEstanA45Grados()
        {
            Assert.That(Posicion.SurOeste.A45Grados(Posicion.NorEste), Is.True);
        }

        [Test]
        public void PosicionesSurEsteYNorEsteNoEstanA45Grados()
        {
            Assert.That(Posicion.SurEste.A45Grados(Posicion.NorEste), Is.False);
        }

        [Test]
        public void PosicionesA135GradosEstanA45Grados()
        {
            var pos = new Posicion(30, 40);
            var pos45Grados = new Posicion(25, 45);
            Assert.That(pos.A45Grados(pos45Grados), Is.True);
        }

        [Test]
        public void PosicionesA225GradosEstanA45Grados()
        {
            var pos = new Posicion(30, 40);
            var pos45Grados = new Posicion(25, 35);
            Assert.That(pos.A45Grados(pos45Grados), Is.True);
        }
    }
}