﻿namespace BatallaNavalgo.Tests.Explosivos.Minas
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves;

    class MinaPuntualConRetardoTests
    {
        [Test]
        public void ALCrearseNoEsExpotable()
        {
            Explosivo mina = new MinaPuntualConRetardo(Posicion.Cero);

            Assert.That(mina.Estado.Explotable, Is.False);
        }

        [Test]
        public void AlCrearseNoEstaExplotada()
        {
            Explosivo mina = new MinaPuntualConRetardo(Posicion.Cero);

            Assert.That(mina.Estado.Explotado, Is.False);
        }

        [Test]
        public void AlAvanzarElEstadoUnaVezSigueSinSerExplotable()
        {
            Explosivo mina = new MinaPuntualConRetardo(Posicion.Cero);

            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotable, Is.False);
        }

        [Test]
        public void AlAvanzarElEstadoDosVecesEsExplotable()
        {
            Explosivo mina = new MinaPuntualConRetardo(Posicion.Cero);

            mina.AvanzarEstado();
            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotable, Is.True);
        }

        [Test]
        public void AlAvanzarElEstadoTresVecesEstaExplotada()
        {
            Explosivo mina = new MinaPuntualConRetardo(Posicion.Cero);

            mina.AvanzarEstado();
            mina.AvanzarEstado();
            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotado, Is.True);
        }

        [Test]
        public void SiNoEstaExplotableNoExplota()
        {
            var lancha = new Lancha(Posicion.Norte, Posicion.Sur, null);
            Explosivo mina1 = new MinaPuntualConRetardo(Posicion.Norte);
            Explosivo mina2 = new MinaPuntualConRetardo(Posicion.Cero);

            mina1.ExplotarEn(lancha);
            mina2.ExplotarEn(lancha);

            Assert.That(lancha.Destruida, Is.False);
        }

        [Test]
        public void SuCostoEs50()
        {
            Explosivo disparo = new MinaPuntualConRetardo(Posicion.Norte);

            Assert.That(disparo.Costo, Is.EqualTo(50));
        }
    }
}