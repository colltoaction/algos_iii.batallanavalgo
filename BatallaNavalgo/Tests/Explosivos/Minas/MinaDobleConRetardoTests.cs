﻿namespace BatallaNavalgo.Tests.Explosivos.Minas
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves;

    class MinaDobleConRetardoTests
    {
        [Test]
        public void ALCrearseNoEsExpotable()
        {
            Explosivo mina = new MinaDobleConRetardo(Posicion.Cero);

            Assert.That(mina.Estado.Explotable, Is.False);
        }

        [Test]
        public void AlCrearseNoEstaExplotada()
        {
            Explosivo mina = new MinaDobleConRetardo(Posicion.Cero);

            Assert.That(mina.Estado.Explotado, Is.False);
        }

        [Test]
        public void AlAvanzarElEstadoUnaVezSigueSinSerExplotable()
        {
            Explosivo mina = new MinaDobleConRetardo(Posicion.Cero);

            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotable, Is.False);
        }

        [Test]
        public void AlAvanzarElEstadoDosVecesEsExplotable()
        {
            Explosivo mina = new MinaDobleConRetardo(Posicion.Cero);

            mina.AvanzarEstado();
            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotable, Is.True);
        }

        [Test]
        public void AlAvanzarElEstadoTresVecesEstaExplotada()
        {
            Explosivo mina = new MinaDobleConRetardo(Posicion.Cero);

            mina.AvanzarEstado();
            mina.AvanzarEstado();
            mina.AvanzarEstado();

            Assert.That(mina.Estado.Explotado, Is.True);
        }

        [Test]
        public void SiNoEstaExplotableNoExplota()
        {
            var lancha = new Lancha(Posicion.Norte, Posicion.Sur, null);
            Explosivo mina1 = new MinaDobleConRetardo(Posicion.Norte);

            mina1.ExplotarEn(lancha);

            Assert.That(lancha.Destruida, Is.False);
        }

        [Test]
        public void SuCostoEs100()
        {
            Explosivo disparo = new MinaDobleConRetardo(Posicion.Norte);

            Assert.That(disparo.Costo, Is.EqualTo(100));
        }
    }
}
