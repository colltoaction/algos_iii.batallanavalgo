namespace BatallaNavalgo.Tests.Explosivos.Minas
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    using BatallaNavalgo.Modelo.Explosivos.Minas;
    using BatallaNavalgo.Modelo.Naves;
    
    [TestFixture]
    public class MinaPorContactoTests
    {

        [Test]
        public void AlCrearseEsExpotable()
        {
            Explosivo mina = new MinaPorContacto(Posicion.Cero);

            Assert.That(mina.Estado.Explotable, Is.True);
        }

        [Test]
        public void SiguienteEstadoSigueSinEstarExplotada()
        {
            Explosivo mina = new MinaPorContacto(Posicion.Norte);
            mina.AvanzarEstado();
            Assert.That(mina.Estado.Explotado, Is.False);
        }

        [Test]
        public void SiguienteEstadoAlExplotarEsExplotada()
        {
            var lancha = new Lancha(Posicion.Norte, Posicion.Sur, null);
            Explosivo mina = new MinaPorContacto(Posicion.Norte);
            mina.ExplotarEn(lancha);
            mina.AvanzarEstado();
            Assert.That(mina.Estado.Explotado, Is.True);
        }

        [Test]
        public void SuCostoEs150()
        {
            Explosivo disparo = new MinaPorContacto(Posicion.Norte);

            Assert.That(disparo.Costo, Is.EqualTo(150));
        }
    }
}

