namespace BatallaNavalgo.Tests.Explosivos
{
    using System;
    using NUnit.Framework;
    using BatallaNavalgo.Modelo;
    using BatallaNavalgo.Modelo.Explosivos;
    
    [TestFixture]
    public class DisparoTests
    {
        [Test]
        public void ALCrearseEsExpotable()
        {
            Explosivo disparo = new Disparo(Posicion.Cero);

            Assert.That(disparo.Estado.Explotable, Is.True);
        }

        [Test]
        public void ALCrearseNoEstaExpotado()
        {
            Explosivo disparo = new Disparo(Posicion.Cero);

            Assert.That(disparo.Estado.Explotado, Is.False);
        }

        [Test]
        public void SiguienteEstadoEstaExplotado()
        {
            Explosivo disparo = new Disparo(Posicion.Norte);
            disparo.AvanzarEstado();
            Assert.That(disparo.Estado.Explotado, Is.True);
        }

        [Test]
        public void SuCostoEs200()
        {
            Explosivo disparo = new Disparo(Posicion.Norte);

            Assert.That(disparo.Costo, Is.EqualTo(200));
        }

        [Test]
        public void EstaExplotadoLuegodeExplotar()
        {
            Explosivo disparo = new Disparo(Posicion.Cero);
            disparo.AvanzarEstado();
            Assert.That(disparo.Explotado, Is.True);
        }
    }
}

